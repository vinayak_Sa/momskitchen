//
//  ProductListing.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 15/03/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import Foundation

struct ListingModel{
    let listingName:String?
    let listingDescription:String?
    let listingCategory: String?
    let listingImage:String?
    let listingVegStatus:Bool
    let listingPrice:Double?
}
