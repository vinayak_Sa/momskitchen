//
//  UserDetails.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 22/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import Foundation


struct UserDetails{
    let userName:String?
    let userEmail:String?
    let profileImageUrl:String?
    let uid:String?
    var chatChannels:[String]?
}


