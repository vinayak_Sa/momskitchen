//
//  DetailsViewController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 21/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit
import GoogleMaps

class ProductController: UIViewController{
    
    var locationManager: CLLocationManager!
    let googleMapView: GMSMapView = {
        let view = GMSMapView()
        return view
    }()
    
    
    // MARK:- Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
         userLocation()
    }
    

    fileprivate func setupNavigation(){
        self.navigationItem.title = "Product"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "back", style: .done, target: nil, action: nil)
    }
    
    // Don't change this
    fileprivate func setupMapView(){
        view.addSubview(googleMapView)
        googleMapView.fillSuperview()
        googleMapView.delegate = self
        googleMapView.settings.myLocationButton = true
        googleMapView.settings.allowScrollGesturesDuringRotateOrZoom = true
        googleMapView.settings.compassButton = true
        googleMapView.settings.zoomGestures = true
    }
    
    fileprivate func setupMaps(locationCoordinate: CLLocationCoordinate2D){
        let update = GMSCameraUpdate.setTarget(locationCoordinate, zoom: 16.0)
        googleMapView.moveCamera(update)

        
        let marker = GMSMarker(position: locationCoordinate)
        marker.title = "My Location"
        marker.snippet = "UK"
        marker.map = googleMapView
    }
}

// MARK:- LocationManagerDelegates
extension ProductController: CLLocationManagerDelegate, GMSMapViewDelegate{
    fileprivate func userLocation(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        if(CLLocationManager.locationServicesEnabled()){
            print("Location Service enabled")
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let coordinate  = manager.location?.coordinate
        let latitude = coordinate?.latitude
        let longitude = coordinate?.longitude
        print("The latitude and logitude of the user is ", latitude ?? 0.00, longitude ?? 0.00)
        guard let coordinates2d = manager.location?.coordinate else { return }
        // change this to the optional binding later on. 
        self.setupMaps(locationCoordinate: (locations.last?.coordinate)!)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("some error with locations", error.localizedDescription)
    }
}

