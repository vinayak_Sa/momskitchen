//
//  SignIn.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 13/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit
import Firebase


class LoginController: UIViewController {
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackgroundBlurImage()
        setupTextFields()
        setupSignUpButton()
    }
    
    
    
    // MARK:- SetupView Methods.
    
    fileprivate func setupBackgroundBlurImage(){
        let backgroundImageView = UIImageView(image: #imageLiteral(resourceName: "brooke-lark-194252-unsplash"))
        view.addSubview(backgroundImageView)
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.clipsToBounds = true
        backgroundImageView.fillSuperview()
        backgroundImageView.addSubview(visualView)
        visualView.fillSuperview()
    }
    
    fileprivate func setupTextFields(){
        let stackView = UIStackView(arrangedSubviews: [emailTextField, passwordTextField, loginButton, forgetPasswordButton
            ])
        stackView.axis = .vertical
        stackView.spacing = 10
      
        view.addSubview(stackView)
        stackView.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 150, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 15, right: view.rightAnchor, paddingRight: -15, height: 0, width: 0, centerX: nil, centerY: nil)
    }
    
    fileprivate func setupSignUpButton(){
        view.addSubview(signUpButton)
        signUpButton.anchor(top: nil, paddingTop: 0, bottom: view.safeAreaLayoutGuide.bottomAnchor, paddingBottom: -10, left: view.leftAnchor, paddingLeft: 15, right: view.rightAnchor, paddingRight: -15, height: 0, width: 0, centerX: nil, centerY: nil)
    }

    // MARK:- objc methods.
    @objc func handleForgetPassword(){
        guard let email = emailTextField.text else { return }
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if let error = error{
                print("Some error ",error.localizedDescription)
                return
            }
            let alertViewController = UIAlertController(title: "Email Sent", message: "We have sent an email to rest your password from our systems", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertViewController.addAction(action)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    @objc func handleLogin(){
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error{
                print("some error",error.localizedDescription)
                return
            }
            // signIn sucess
            self.present(MainTabBarController(), animated: true, completion: nil)
        }
    }
    
    @objc func handleSignup(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK:- Elements
    let emailTextField: CustomTextField = {
        let tf = CustomTextField(padding: 10)
        tf.placeholder = "Enter the email"
        tf.backgroundColor = UIColor.color(r: 240, g: 240, b: 240)
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        return tf
    }()
    
    let passwordTextField: CustomTextField = {
        let tf = CustomTextField(padding: 10)
        tf.placeholder = "Enter the password"
        tf.backgroundColor = UIColor.color(r: 240, g: 240, b: 240)
        tf.isSecureTextEntry = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        return tf
    }()
    
    
    let loginButton: UIButton = {
        let bv = UIButton(type: .system)
        bv.setTitle("Login", for: .normal)
        bv.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        bv.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        bv.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        bv.translatesAutoresizingMaskIntoConstraints = false
        bv.heightAnchor.constraint(equalToConstant: 50).isActive = true
        bv.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        bv.layer.cornerRadius = 5
        return bv
    }()
    
    
    
    let signUpButton: UIButton = {
        let bv = UIButton(type: .system)
        bv.setTitle("Create an account.", for: .normal)
        bv.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        bv.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        bv.addTarget(self, action: #selector(handleSignup), for: .touchUpInside)
        return bv
    }()
    
    let forgetPasswordButton: UIButton = {
        let bv = UIButton(type: .system)
        bv.setTitle("Forgot your password ?", for: .normal)
        bv.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        bv.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        bv.addTarget(self, action: #selector(handleForgetPassword), for: .touchUpInside)
        return bv
    }()
    
    let visualView: UIVisualEffectView = {
        let blurrEffect = UIBlurEffect(style: .light)
        let effectView = UIVisualEffectView(effect: blurrEffect)
        effectView.alpha = 0.6
        return effectView
    }()

}
