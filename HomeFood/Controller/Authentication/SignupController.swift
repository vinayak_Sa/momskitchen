//
//  ViewController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 13/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit
import Firebase
import JGProgressHUD

class SignupController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        let window = UIApplication.shared.keyWindow
        if (Auth.auth().currentUser != nil){
            window?.rootViewController = MainTabBarController()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationSettings()
        setupBackgroundBlurImage()
        setupTextFields()
        setupTapGesture()
        setupLoginButton()
    }
    
    // MARK:- Navigation Settings.
    fileprivate func navigationSettings(){
        self.navigationController?.isNavigationBarHidden = true
    }

    //MARK:- Setup Elements.
    fileprivate func setupTextFields(){
        view.addSubview(photoSelectButton)
        photoSelectButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 35, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, height: 200, width: 200, centerX: view.centerXAnchor, centerY: nil)
        
        let stackView = UIStackView(arrangedSubviews: [userNameTextField ,emailTextField, passwordTextField, signUpButton])
        
        stackView.axis = .vertical
        stackView.spacing = 5
        view.addSubview(stackView)
        print("Added to stackView")
        
        stackView.anchor(top: photoSelectButton.bottomAnchor, paddingTop: 10, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 10, right: view.rightAnchor, paddingRight: -15, height: 0, width: 0, centerX: nil, centerY: nil)
    }
    
    // need to change this.
    fileprivate func setupBackgroundBlurImage(){
        let backgroundImageView = UIImageView(image: #imageLiteral(resourceName: "bg"))
        view.addSubview(backgroundImageView)
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.clipsToBounds = true
        backgroundImageView.fillSuperview()
        backgroundImageView.addSubview(visualView)
        visualView.fillSuperview()
    }
    
    
    fileprivate func setupTapGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    fileprivate func setupLoginButton(){
        self.view.addSubview(loginButton)
        loginButton.anchor(top: nil, paddingTop: 0, bottom: view.safeAreaLayoutGuide.bottomAnchor, paddingBottom: -10, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 0, height: 0, width: 0, centerX: nil, centerY: nil)
    }
    
    
    // MARK:- @objc function.
    @objc func handleTapGesture(){
        print("Trying to hide ")
        self.view.endEditing(true)
    }
    
    @objc func handleSelectImage(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    @objc func handleSignup(){
        guard let email = emailTextField.text else{ return }
        guard let password = passwordTextField.text else { return }
        signUpUser(email: email, password: password)
    }
    
    @objc func handleForgetPassword(){
        print("Forgot password")
    }
    
    @objc func handleLoginTapped(){
        let loginController = LoginController()
        self.navigationController?.pushViewController(loginController, animated: true)
    }
    
    
    // MARK:- Firebase Methods.
    
    fileprivate func signUpUser(email:String, password: String){
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let error = error{
                self.hud.textLabel.text = "Error Registering user"
                self.hud.detailTextLabel.text = "\(error.localizedDescription)"
                self.hud.show(in: self.view)
                self.hud.dismiss(afterDelay: 2, animated: true)
                return
            }
            print("New user is created")
            // Now need to save the images to the firebase database.
            let uid = UUID().uuidString
            let ref = Storage.storage().reference(withPath: "profileImages")
            guard let data = self.photoSelectButton.imageView?.image?.jpegData(compressionQuality: 0.4) else { return }
            let imageProfile = ref.child(uid)
            imageProfile.putData(data, metadata: nil, completion: { (storageData, error) in
                if let error = error{
                    print("Some error ", error.localizedDescription)
                    return
                }
                
                imageProfile.downloadURL(completion: { (url, error) in
                    if let error = error{
                        print("Some error", error.localizedDescription)
                        return
                    }
                    
                    // Got the uploaded image url from firebase.
                    print("Got the UploadedimageUrl",url?.absoluteString ?? "")
                    
                    
                    // From this point saving to the Firestore Database.
                    let userID = Auth.auth().currentUser?.uid ?? ""
                    let userEmail = Auth.auth().currentUser?.email
                    let userName = self.userNameTextField.text ?? "No Username"
                    let userProfileimageUrl = url?.absoluteString ?? ""
                    
                    let data: [String: Any] = ["UID": userID, "UserEmail": userEmail ?? "", "MessageChannels": [String](), "ProfileImage": userProfileimageUrl, "Username": userName]
                    
                    
                    Firestore.firestore().collection("UserDetails").document(userID).setData(data, completion: { (error) in
                        if let error = error{
                            print("Some errors ", error.localizedDescription)
                            return
                        }
                    })
                    
                })
                self.present(MainTabBarController(), animated: true, completion: nil)
                print("Saved the details")
            })
        }
    }
    
    
    
    // MARK:- Elements.
    let hud: JGProgressHUD = {
        let hud = JGProgressHUD(style: .dark)
        return hud
    }()
    
    let emailTextField: CustomTextField = {
        let tf = CustomTextField(padding: 10)
        tf.placeholder = "Enter the email"
        tf.backgroundColor = UIColor.color(r: 240, g: 240, b: 240)
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        return tf
    }()
    
    let userNameTextField: CustomTextField = {
        let tf = CustomTextField(padding: 10)
        tf.placeholder = "Enter the username"
        tf.backgroundColor = UIColor.color(r: 240, g: 240, b: 240)
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        return tf
    }()
    
    let passwordTextField: CustomTextField = {
        let tf = CustomTextField(padding: 10)
        tf.placeholder = "Enter the password"
        tf.backgroundColor = UIColor.color(r: 240, g: 240, b: 240)
        tf.isSecureTextEntry = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        return tf
    }()
    
    let photoSelectButton:UIButton = {
        let bv = UIButton(type: .system)
        bv.setTitle("Profile Image", for: .normal)
        bv.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        bv.translatesAutoresizingMaskIntoConstraints = false
        bv.layer.cornerRadius = 100
        bv.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        bv.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        bv.clipsToBounds = true
        bv.contentMode = .scaleAspectFit
        bv.addTarget(self, action: #selector(handleSelectImage), for: .touchUpInside)
        return bv
    }()
    
    let signUpButton: UIButton = {
        let bv = UIButton(type: .system)
        bv.setTitle("SignUp", for: .normal)
        bv.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        bv.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        bv.addTarget(self, action: #selector(handleSignup), for: .touchUpInside)
        bv.translatesAutoresizingMaskIntoConstraints = false
        bv.heightAnchor.constraint(equalToConstant: 50).isActive = true
        bv.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        bv.layer.cornerRadius = 5
        return bv
    }()
    

    
    
    let loginButton: UIButton = {
        let bv = UIButton(type: .system)
        bv.setTitle("Already have an account.", for: .normal)
        bv.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        bv.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        bv.addTarget(self, action: #selector(handleLoginTapped), for: .touchUpInside)
        return bv
    }()
    
    let visualView: UIVisualEffectView = {
        let blurrEffect = UIBlurEffect(style: .light)
        let effectView = UIVisualEffectView(effect: blurrEffect)
        effectView.alpha = 0.6
        return effectView
    }()
    
}


//MARK:- ImgePickerController

extension SignupController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        self.photoSelectButton.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
        
            dismiss(animated: true, completion: nil)
    }
}

