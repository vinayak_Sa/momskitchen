//
//  PostOrderController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 19/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit
import DropDown
import Firebase

// Needs the discussion on this with the group and so on .....</>

class AddListingController: UIViewController{
    
    let productImageButton: UIButton = {
        let bv = UIButton(type: .system)
        bv.backgroundColor = UIColor.color(r: 240, g: 240, b: 240)
        bv.layer.cornerRadius = 5
        bv.contentMode = .scaleAspectFit
        bv.clipsToBounds = true
        bv.addTarget(self, action: #selector(handleSelectProductImage), for: .touchUpInside)
        return bv
    }()
    
    
    let photoImageSubViewLabel: UILabel = {
        let label = UILabel()
        label.text = "Photo"
        label.tag = 1234
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        return label
    }()
    
    
    let photoImageSubViewImageView: UIImageView = {
         let iv = UIImageView()
         iv.image = #imageLiteral(resourceName: "camera").withRenderingMode(.alwaysOriginal)
         iv.tag = 2345
         return iv
        
    }()
    
    
    let photoImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        return iv 
    }()
    
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let informationView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9384416938, green: 0.9356508851, blue: 0.9556260705, alpha: 1)
        view.layer.cornerRadius = 5
        
        return view
    }()
    
    
    
    let titleTextField: CustomTextField = {
        let titleView = CustomTextField(padding: 10)
        titleView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        titleView.font = UIFont.systemFont(ofSize: 16)
        titleView.autocorrectionType = .no
        titleView.placeholder = "Food Category"
        return titleView
    }()
    
    let lineView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        return view
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Description"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = #colorLiteral(red: 1, green: 0.6256141067, blue: 0, alpha: 1)
        return label
    }()
    
    let vegetarianButton: VegetarianStatusButton = {
        let vegetarianButton = VegetarianStatusButton(colorOn: #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), colorOff: .clear, titleOn: "Vegetarian", titleOff: "Vegetarian", titleColorOn: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), titleColorOff: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        vegetarianButton?.setTitle("Vegetarian", for: .normal)
        vegetarianButton?.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        vegetarianButton?.layer.borderWidth = 1
        return vegetarianButton ?? UIButton() as! VegetarianStatusButton  // not a good practice.
    }()
    
  
    let nonVegetarianButton: VegetarianStatusButton = {
        let nonVegetarianButton = VegetarianStatusButton(colorOn: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), colorOff: .clear, titleOn: "Non-Vegetarian", titleOff: "Non-Vegetarian", titleColorOn: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), titleColorOff: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        nonVegetarianButton?.setTitle("Non-Vegetarian", for: .normal)
        nonVegetarianButton?.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        nonVegetarianButton?.layer.borderWidth = 1
        return nonVegetarianButton ?? UIButton() as! VegetarianStatusButton
    }()
    
    
    let buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        return stackView
    }()
    
    
    let dp: DropDown = {
        let dv = DropDown()
        dv.dataSource = ["Asian", "French", "Home Cooked", "Other"] // needs to be changed.
        dv.width = 200
        return dv
    }()
    let categoryLabel: UILabel = {
        let lv = UILabel()
        lv.text = "Food Category"
        return lv
    }()
    
    
    let categoryField:CustomTextField = {
        let cv = CustomTextField(padding: 10)
        cv.placeholder = "Select a category"
        cv.backgroundColor = UIColor.color(r: 240, g: 240, b: 240)
        return cv
    }()
    
    let descriptionTextView: UITextView = UITextView()
    
    let addListingButton: UIButton = {
        let listingButton = UIButton(type: .system)
        listingButton.setTitle("Add Listing", for: .normal)
        listingButton.setBackgroundColor(color: #colorLiteral(red: 1, green: 0.6256141067, blue: 0, alpha: 1), forState: .normal)
        listingButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        listingButton.addTarget(self, action: #selector(handleAddListing), for: .touchUpInside)
        return listingButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        setupNavigation()
        setupScrollView()
        setupProductImageViewButton()
        setupVegetarianStatus()
        setupInformation()
        setupCategoryDropDown()
        setupaddListingButton()
    }
    
    fileprivate func setupVegetarianStatus(){
        let vegStatusbuttonViews = [vegetarianButton, nonVegetarianButton]
        vegStatusbuttonViews.forEach { (vegetarianStatusButton) in
            buttonsStackView.addArrangedSubview(vegetarianStatusButton)
        }
        
        buttonsStackView.spacing = 7
        buttonsStackView.axis = .horizontal
        buttonsStackView.distribution = .fillEqually
        view.addSubview(buttonsStackView)
        buttonsStackView.anchor(top: productImageButton.bottomAnchor, paddingTop: 20, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 10, right: view.rightAnchor, paddingRight: -10, height: 45, width: 0, centerX: nil, centerY: nil)
        
    }
    
    
    fileprivate func setupInformation(){
        view.addSubview(informationView)
        informationView.anchor(top: buttonsStackView.bottomAnchor, paddingTop: 15, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 10, right: view.rightAnchor, paddingRight: -10, height: 400, width: 0, centerX: nil, centerY: nil)
        
        let foodTitleLabel:UILabel = UILabel()
        foodTitleLabel.text = "Food Item Name"
        foodTitleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        foodTitleLabel.textColor = #colorLiteral(red: 1, green: 0.6256141067, blue: 0, alpha: 1)
        informationView.addSubview(foodTitleLabel)
        foodTitleLabel.anchor(top: informationView.topAnchor, paddingTop: 10, bottom: nil, paddingBottom: 0, left: informationView.leftAnchor, paddingLeft: 10, right: informationView.rightAnchor, paddingRight: -10, height: 40, width: 0, centerX: nil, centerY: nil)
        
        informationView.addSubview(titleTextField)
    
        
        titleTextField.anchor(top: foodTitleLabel.bottomAnchor, paddingTop: 10, bottom: nil, paddingBottom: 0, left: informationView.leftAnchor, paddingLeft: 10, right: informationView.rightAnchor, paddingRight: -10, height: 40, width: 0, centerX: nil, centerY: nil)
        informationView.addSubview(lineView)
        lineView.anchor(top: titleTextField.bottomAnchor, paddingTop: 10, bottom: nil, paddingBottom: 0, left: informationView.leftAnchor, paddingLeft: 10, right: informationView.rightAnchor, paddingRight: -10, height: 2, width: 0, centerX: nil, centerY: nil)
        
        informationView.addSubview(descriptionLabel)
        descriptionLabel.anchor(top: lineView.bottomAnchor, paddingTop: 15, bottom: nil, paddingBottom: 0, left: informationView.leftAnchor, paddingLeft: 15, right: informationView.rightAnchor, paddingRight: -15, height: 0, width: 0, centerX: nil, centerY: nil)
        
        descriptionTextView.autocapitalizationType = .none
        descriptionTextView.autocorrectionType = .no
        informationView.addSubview(descriptionTextView)
        descriptionTextView.anchor(top: descriptionLabel.bottomAnchor, paddingTop: 15, bottom: informationView.bottomAnchor, paddingBottom: -15, left: informationView.leftAnchor, paddingLeft: 15, right: informationView.rightAnchor, paddingRight: -15, height: 0, width: 0, centerX: nil, centerY: nil)
        descriptionTextView.backgroundColor = #colorLiteral(red: 0.8275127009, green: 0.8262191381, blue: 0.8488144575, alpha: 1)
//        descriptionTextView.layer.borderWidth = 2
//        descriptionTextView.layer.borderColor = #colorLiteral(red: 1, green: 0.6256141067, blue: 0, alpha: 1)
        descriptionTextView.layer.cornerRadius = 5
        descriptionTextView.font = UIFont.systemFont(ofSize: 16)
    }
    
    fileprivate func setupScrollView(){
        view.addSubview(scrollView)
        scrollView.anchor(top: view.topAnchor, paddingTop: 0, bottom: view.bottomAnchor, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 0, height: 0, width: 0, centerX: nil, centerY: nil)
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: 840)

    }
    
    fileprivate func setupNavigation(){
        self.navigationItem.title = "Add Listing"
    }
    
    fileprivate func setupProductImageViewButton(){
        productImageButton.addSubview(photoImageSubViewLabel)
        photoImageSubViewLabel.anchor(top: productImageButton.topAnchor, paddingTop: 20, bottom: nil, paddingBottom: 0, left: productImageButton.leftAnchor, paddingLeft: 15, right: nil, paddingRight: 0, height: 0, width: 0, centerX: nil, centerY: nil)
        productImageButton.addSubview(photoImageSubViewImageView)
        
        photoImageSubViewImageView.anchor(top: productImageButton.topAnchor, paddingTop: 15, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: productImageButton.rightAnchor, paddingRight: -20, height: 40, width: 40, centerX: nil, centerY: nil)

        // here we add the overall view.
        scrollView.addSubview(productImageButton)
        productImageButton.anchor(top: scrollView.topAnchor, paddingTop: 15, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 15, right: view.rightAnchor, paddingRight: -15, height: 150, width: 0, centerX: nil, centerY: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    // need to make changes to this dropdown features. here 
    fileprivate func setupCategoryDropDown(){
        let categoryLabel = UILabel()
        categoryLabel.text = "Choose from categories"
        categoryLabel.font = UIFont.boldSystemFont(ofSize:16)
        categoryLabel.textColor = #colorLiteral(red: 1, green: 0.6256141067, blue: 0, alpha: 1)
        view.addSubview(categoryLabel)
        categoryLabel.anchor(top: informationView.bottomAnchor, paddingTop: 10, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 15, right: view.rightAnchor, paddingRight: -15, height: 0, width: 0, centerX: nil, centerY: nil)
        
        view.addSubview(categoryField)
        categoryField.anchor(top: categoryLabel.bottomAnchor, paddingTop: 15, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 15, right: view.rightAnchor, paddingRight: -15, height: 45, width: 0, centerX: nil, centerY: nil)
        dp.anchorView = categoryField
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        categoryField.addGestureRecognizer(tapGesture)
    }
    
    
    fileprivate func setupaddListingButton(){
        view.addSubview(addListingButton)
        addListingButton.anchor(top: categoryField.bottomAnchor, paddingTop: 15, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 15, right: view.rightAnchor, paddingRight: -15, height: 56, width: 0, centerX: nil, centerY: nil)
    }
    
    // MARK:- @OBJC Methods.
    @objc func handleTap(){
        dp.show()
        dp.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.categoryField.text = item
        }

    }
    
    @objc func handleSelectProductImage(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    @objc func handleAddListing(){
        guard let image = productImageButton.imageView?.image else { return }
        let vegStatus = vegetarianButton.isOn ? true : false
        guard let foodTitle = titleTextField.text else { return }
        guard let foodDescription = descriptionTextView.text else { return }
        guard let foodCategory = categoryField.text else { return }
        // store this image
        let ref = Storage.storage().reference(withPath: "ProductImages")
        let productImageUid = UUID().uuidString
        let refchild = ref.child(productImageUid)
        guard let data = image.jpegData(compressionQuality: 0.4) else { return }
        refchild.putData(data, metadata: nil) { (storagemd, error) in
            if let error = error{
                print("Some error uploading File", error.localizedDescription)
                return
            }
            refchild.downloadURL(completion: { (url, error) in
                if let error = error{
                    print("Some error getting downloading url", error.localizedDescription)
                }
                print("The downloading url is ", url?.absoluteString ?? "")
                let productImageUrl = url?.absoluteString ?? ""
                // here updates the data. - to Firestore 2 places.
                // 1. UserProfile.
                // 2. New Collection - Listings
                let listingRef = Firestore.firestore().collection("Listings")
                let documentUid = UUID().uuidString
                listingRef.document(documentUid)
                
                let data: [String: Any] = ["ProductImageUrl": productImageUrl, "VegStatus": vegStatus, "FoodTitle": foodTitle, "FoodDescription": foodDescription, "FoodCategory": foodCategory]
                listingRef.addDocument(data: data, completion: { (error) in
                    print("Error updating the data")
                    return
                })
                print("Updated the data")
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
}


// MARK:- UIImagePickerDelegate Methods.

extension AddListingController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        if let labelView = self.productImageButton.viewWithTag(1234){
            labelView.removeFromSuperview()
        }
        
        if let imageView = self.productImageButton.viewWithTag(2345){
            imageView.removeFromSuperview()
        }
        self.productImageButton.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
