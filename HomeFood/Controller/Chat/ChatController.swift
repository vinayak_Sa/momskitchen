//
//  ChatController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 26/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit
import Firebase


class ChatController: UICollectionViewController{

    var recipentUid:String?
    var messageUid:String?
    var currentUserMessages:[String]?
    
    // MARK:- Elements
    let messageTextField: CustomTextField = {
        let tf = CustomTextField(padding: 10)
        tf.placeholder = "Enter the message"
        tf.backgroundColor = UIColor.color(r: 240, g: 240, b: 240)
        return tf
    }()
    
    
    let sendButton: UIButton = {
        let bv = UIButton(type: .system)
        bv.setImage(#imageLiteral(resourceName: "send").withRenderingMode(.alwaysOriginal), for: .normal)
        bv.addTarget(self, action: #selector(handleSendButton), for: .touchUpInside)
        return bv
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewSettings()
        setupTextField()
        setupNavigation()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
         self.tabBarController?.tabBar.isHidden = false
    }
    
    fileprivate func collectionViewSettings(){
       collectionView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    
    fileprivate func setupTextField(){
        let stackView = UIStackView(arrangedSubviews: [messageTextField, sendButton])
        stackView.backgroundColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        stackView.axis = .horizontal
  	
        stackView.spacing = 0
        stackView.isLayoutMarginsRelativeArrangement = true
        
        self.view.addSubview(stackView)
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        stackView.anchor(top: nil, paddingTop: 0, bottom: view.safeAreaLayoutGuide.bottomAnchor, paddingBottom: 3, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 3, height: 50, width: 0, centerX: nil, centerY: nil)
        
        print("added stack view")
    }
    
    
    fileprivate func setupNavigation(){
        self.navigationItem.title = "Chat"
    }
    
    
    //very close to making the chatting system and need something to do here.
    func updateMessage(message:String){
        let currentUserUid = Auth.auth().currentUser?.uid
        let messagesRef = Firestore.firestore().collection("Messages")
        let userRef = Firestore.firestore().collection("UserDetails")
        
        let data = ["Message": message]
        
        // check if message exist in currentMessages.
        
        if let messageUid = messageUid{
            // document to message.
            messagesRef.document(messageUid).updateData(["Messages" : FieldValue.arrayUnion([message])])
        }else{
            messageUid = UUID().uuidString
            messagesRef.document(messageUid ?? "").setData(data, merge: true) { (error) in
                if let error = error{
                    print("Some error saving message to chat channel", error.localizedDescription)
                    return
                }
                print("Saved message to chat chanel.")
            }
        }
        
        
        
       
        messagesRef.document(currentUserUid ?? "").setData(data, merge: true) { (error) in
            if let error = error{
                print("Some error ", error.localizedDescription)
                return
            }
            // some updates here.
        }
        
    }
    
    
    @objc func handleSendButton(){
        guard let message = messageTextField.text else { return }
        updateMessage(message: message)
    }
}
