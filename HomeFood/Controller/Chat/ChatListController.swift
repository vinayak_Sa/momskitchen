//
//  ChatListController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 27/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit
import Firebase

class ChatListController: UITableViewController{
    // this needs to be the 
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        setupNavigation()
        setupTableView()
        downloadUsers()
    }
    
    var users: [UserDetails] = [UserDetails]()
    
    fileprivate let cellId = "customCell"
    
    // need to change this make some query changes to only show the filtered list.
    // this query should be realtime.
    fileprivate func downloadUsers(){
        Firestore.firestore().collection("UserDetails").getDocuments { (snapshot, error) in
            if let error = error{
                print("Some error fetching stuff", error.localizedDescription)
                return
            }
            print("The value of snapshot is ", snapshot?.documentChanges ?? "")
            let documents = snapshot?.documents
            documents?.forEach({ (document) in
                let data = document.data()
                guard let username = data["Username"] as? String else {return}
                guard let userEmail = data["UserEmail"] as? String else {return}
                guard let profileImage = data["ProfileImage"] as? String else {return}
                guard let userUid = data["UID"] as? String else { return }
                // there is bug here if the user doesnot upload the image.
                let details = UserDetails.init(userName: username, userEmail: userEmail, profileImageUrl: profileImage, uid: userUid, chatChannels: nil)
                self.users.append(details)
                self.tableView.reloadData()
            })
        }
    }
    
    
    fileprivate func setupTableView(){
        tableView.register(ChatListCell.self, forCellReuseIdentifier: "customCell")
        let nib = UINib(nibName: "ChatListCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellId)
        tableView.rowHeight = 105
    }
    
    fileprivate func setupNavigation(){
        self.navigationItem.title = "Messaging"
    }
    
    
    // Fix - polluting this protocol method.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ChatListCell
        cell.profileImageCornerRadius()
        cell.userName.text = users[indexPath.row].userName
        cell.email.text = users[indexPath.row].userEmail
        let imageUrl = URL(string: users[indexPath.row].profileImageUrl ?? "")
        cell.profileImage.sd_setImage(with: imageUrl, completed: nil)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let layout = UICollectionViewFlowLayout()
        let chatController = ChatController(collectionViewLayout: layout)
        chatController.recipentUid = users[indexPath.row].uid
       
        
        self.navigationController?.pushViewController(chatController, animated: true)
        print("Selected \(indexPath.row)")
    }
    
}
