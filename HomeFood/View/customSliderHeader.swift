//
//  CustomHeaderCell.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 19/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit

class customSliderHeader: UICollectionViewCell{
    
    let headerImages: [UIImage] = [#imageLiteral(resourceName: "2"), #imageLiteral(resourceName: "3"),#imageLiteral(resourceName: "Inidan"), #imageLiteral(resourceName: "food")]
    
    let customCollectionView:UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.backgroundColor  = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return cv
    }()
    
    // MARK:- Init Method.
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        setupTopFoodCollectionView()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("Some error")
    }
    
    // MARK:- Setup Element
    
    fileprivate func setupTopFoodCollectionView(){
        customCollectionView.register(CustomTopCell.self, forCellWithReuseIdentifier: "cellId")
        
        addSubview(customCollectionView)
        customCollectionView.showsHorizontalScrollIndicator = false
        customCollectionView.fillSuperview()
        customCollectionView.delegate = self
        customCollectionView.dataSource = self
        customCollectionView.isPagingEnabled = true
        let layout = customCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = .horizontal
       
    }
    
    
}


// MARK:- CustomCollectionViewTop10

extension customSliderHeader: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! CustomTopCell
        cell.topFoodImageView.image = headerImages[indexPath.row]
        cell.pageControl.currentPage = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: 300)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


class CustomTopCell: UICollectionViewCell{
    
    let topFoodImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 1
        pc.numberOfPages = 4
        pc.currentPageIndicatorTintColor = UIColor.color(r: 249, g: 152, b: 58)
           // #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        pc.pageIndicatorTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        return pc
    }()
    
    
    let effectView: UIVisualEffectView = {
        let blurrEffect = UIBlurEffect(style: .dark)
        let effectView = UIVisualEffectView(effect: blurrEffect)
        effectView.alpha = 0.4
        return effectView
    }()
    
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        
     
        
        addSubview(topFoodImageView)
        topFoodImageView.fillSuperview()
        
        self.addSubview(effectView)
        effectView.fillSuperview()
        
        addSubview(pageControl)

        pageControl.anchor(top: nil, paddingTop: 0, bottom: self.bottomAnchor, paddingBottom: -10, left: nil, paddingLeft: 10, right: nil, paddingRight: 0, height: 50, width: 0, centerX: self.centerXAnchor, centerY: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("Some error")
    }
    
}
