//
//  ResturantsCell.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 17/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit

class RestaurantsCell: UICollectionViewCell{

    var currentListing: ListingModel?{
        didSet{
            let image = currentListing?.listingImage
            let title = currentListing?.listingName
            guard let finalIUmageUrl = URL(string: image ?? "") else { return }
            self.productImage.sd_setImage(with: finalIUmageUrl, completed: nil)
            self.productTitle.text = title ?? ""
        }
    }
    
    @IBOutlet weak var timeb: UIButton!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productVendor: UILabel!
  
    
    
}
